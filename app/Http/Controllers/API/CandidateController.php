<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Candidate;

class CandidateController extends Controller
{
    public function index(Request $request){
        $search = $request->search;
        $sort = $request->sort;
        $paginate = $request->paginate;
        $query = Candidate::query();
        if (isset($search) && isset($sort) && isset($paginate)) {
            $query = $query->where('name','LIKE', '%'. $search .'%')->orderBy('id',$sort)->paginate($paginate);
        } else if(isset($search) && isset($sort)){
            $query = $query->where('name','LIKE', '%'. $search .'%')->orderBy('id',$sort)->get();
        } else if(isset($search) && isset($paginate)){
            $query = $query->where('name','LIKE', '%'. $search .'%')->orderBy('id','desc')->paginate($paginate);
        }else if(isset($sort) && isset($paginate)){
            $query = $query->orderBy('id',$sort)->paginate($paginate);
        }else if (isset($search)) {
            $query = $query->where('name','LIKE', '%'. $search .'%')->orderBy('id','desc')->get();
        }else if (isset($sort)) {
            $query = $query->$query->orderBy('id',$sort)->get();
        }else if (isset($paginate)) {
            $query = $query->orderBy('id','desc')->paginate($paginate);
        }else{
            $query = $query->orderBy('id','desc')->get();
        }
        
        return response()->json($query,200);
    }
    public function edit($id){
        $candidate = Candidate::find($id);
        if ($candidate) {
            return response()->json($candidate, 200);
        }else{
            return response()->json($candidate, 404);
        }
    }
    public function update(Request $request,$id){
        $candidate = Candidate::find($id);
        if ($candidate) {
            $candidate->name = $request->name;
            $candidate->phone_number = $request->phone_number;
            $candidate->email = $request->email;
            $candidate->address = $request->address;
            $saved = $candidate->save();
            
            return response()->json($candidate, 200);
            
        }else{
            return response()->json($candidate, 404);
        }
    }
}
